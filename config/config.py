class Config:
    BASE_URL = 'https://yandex.ru/'
    TIME_WAIT = 20  # Time to wait for the visibility of the element
    HOME_PAGE_TITLE = "Яндекс"
    IMAGES_SEARCH_URL = "https://yandex.ru/images/"
